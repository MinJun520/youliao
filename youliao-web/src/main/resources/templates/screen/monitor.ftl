<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<style>
    body{font-family: Helvetica, Tahoma, Arial,"Microsoft YaHei", "微软雅黑", SimSun, "宋体", Heiti, "黑体", sans-serif;margin: 0px;padding: 20px;background-color: #0e1115;font-size: 15px;width: calc(1024px - 40px); font-weight: bold}
    .marginT25{margin-top: 25px;}
    .font20{font-size: 20px;}
    .font16{font-size: 16px;}
    .c_green{color: #49cc4b;}
    .c_white{color: #edeeee;}
    .c_blue{color: #1baafa;}
    .c_gray{color: #738397;}
    .c_red{color: #f1263e;}
    .t_c{text-align: center;}
    .t_r{text-align: right;}

    #timeBox{width: 100px;display: inline-block;}
    .left_box{float: left;width: 130px;height: 485px;background-color: #171c23;}
    .left_box_tit{height: 45px;line-height: 45px;background-color: #222934;}
    .left_box_tit img{position: relative;top: 2px;}
    .left_box ul{padding: 0px;}
    .left_box ul li{list-style: none;height: 34.5px;line-height: 34.5px;}
    .right_box{display: inline-block;width: 840px;height: 485px;overflow: hidden;background-color: #171c23;}
    .right_box table{width: 100%;border-collapse: collapse;}
    .right_box table thead td{background-color: #222934;border-left: 1px solid #12161c;height: 43px;}
    .right_box table thead td.no_db{border-left: none;}
    .right_box table tbody td{height: 42px;}
    .right_box table tbody .two{background-color: #1d232b;}
</style>
<body>
<div class="c_green font20 t_r"><span id="dataBox"></span><span id="timeBox"></span></div>
<div class="t_r marginT25">
    <div class="left_box t_c">
        <div class="c_white left_box_tit font16"> 叫号中车辆</div>
        <!--12条数据-->
        <ul class="c_green" id="carList"></ul>
    </div>
    <div class="right_box t_c">
        <!--10条数据-->
        <table>
            <thead>
            <tr class="c_white font16">
                <td width="150" class="no_db">线路</td>
                <td width="80">车牌号</td>
                <td width="110">工厂</td>
                <td width="100">仓库</td>
                <td width="100">门卫室</td>
                <td width="120">物料</td>
                <td width="80">状态</td>
                <td>前方车辆</td>
            </tr>
            </thead>
            <tbody id="listBox">

            </tbody>
        </table>
    </div>
</div>
</body>
</html>
<script src="/api/js/jquery-3.2.1.min.js"></script>
<script>
    showTime();
    //当前时间
    function showTime(){
        var today = new Date();
        //分别取出年、月、日、时、分、秒
        var year = today.getFullYear();
        var month = today.getMonth()+1;
        var day = today.getDate();
        var hours = today.getHours();
        var minutes = today.getMinutes();
        var seconds = today.getSeconds();
        //如果是单个数，则前面补0
        month  = month<10  ? "0"+month : month;
        day  = day <10  ? "0"+day : day;
        hours  = hours<10  ? "0"+hours : hours;
        minutes = minutes<10 ? "0"+minutes : minutes;
        seconds = seconds<10 ? "0"+seconds : seconds;
        $("#dataBox").text(year+"年"+month+"月"+day+"日");
        $("#timeBox").text(hours+":"+minutes+":"+seconds);
        //延时器
        window.setTimeout("showTime()",1000);
    }
</script>


<script language="javascript"type="text/javascript">
    var wsUri ="ws://192.168.0.111:8080/api/screenws/"+123;
    websocket = new WebSocket(wsUri);
    websocket.onopen = function(evt) {
        console.log('链接开通····');
        console.log(evt)
    };
    websocket.onmessage = function(evt) {
        console.log('`````接收数据中`````');

        websocket.send("Hello server!");
        var theData = JSON.parse(evt.data);
        if(theData.type == 'data'){//显示叫号列表
            var showData = JSON.parse(theData.data);
            console.log(showData)
            var htmlStr = '';
            for(var i=0;i<showData.length;i++){
                iData = showData[i];
                var classStr = '';
                var classStr2 = '';
                if(i%2 == 0){
                    classStr = 'two';
                }
                /*0=等待，1=正在入场，2=已入场，3,=已出厂。4=已过号*/
                if(iData.status == 0){//等待
                    htmlStr += '<tr class="c_blue '+classStr+'">';
                }else if(iData.status == 4){//已过号
                    htmlStr += '<tr class="c_gray '+classStr+'">';
                    classStr2 = 'c_red';
                }else{
                    htmlStr += '<tr class="c_green '+classStr+'">';
                }
                htmlStr +='<td>'+iData.startAddr+'-'+iData.endAddr+'</td>'+
                        '<td>'+iData.carNo+'</td>'+
                        '<td>'+iData.factoryName+'</td>'+
                        '<td>'+iData.warehouseName+'</td>'+
                        '<td>'+iData.guardName+'</td>'+
                        '<td>'+fomat(iData.skuName)+'</td>'+
                        '<td class="'+classStr2+'">'+iData.statusName+'</td>'+
                        '<td>'+iData.waitNum+'</td>'+
                        '</tr>';
            }
            $("#listBox").children().remove();
            $("#listBox").append(htmlStr);
        }
        if(theData.type == 'carList'){//显示车辆列表
            var showData = JSON.parse(theData.data);
            var htmlStr = '';
            for(var i=0;i<showData.length;i++){
                htmlStr += '<li>'+showData[i]+'</li>';
            }
            $("#carList").children().remove();
            $("#carList").append(htmlStr);
        }
    };
    websocket.onerror = function(evt) {
        console.error('链接错误····');
        console.log(evt)
    };
    websocket.onclose = function(evt) {
        console.log('链接关闭····');
        console.log(evt)
    };

    function fomat(obj) {
        if(obj==undefined||obj==null){
            return ""
        }else {
            return obj;
        }

    }
</script>
